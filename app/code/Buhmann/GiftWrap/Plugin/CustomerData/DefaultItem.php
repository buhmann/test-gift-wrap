<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Buhmann\GiftWrap\Plugin\CustomerData;

use Magento\Quote\Model\Quote\Item;

/**
 * Default item
 */
class DefaultItem
{
    public function afterGetItemData(
        \Magento\Checkout\CustomerData\AbstractItem $subject,
        $result,
        Item $item
    ){
        return \array_merge(
            ['gift_wrap' => $item->getGiftWrap()],
            $result
        );
    }
}