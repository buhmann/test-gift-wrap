<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Buhmann\GiftWrap\Observer;

use Magento\Framework\Event\ObserverInterface;

class CheckoutCartUpdateItemsBefore implements ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $cart = $observer->getEvent()->getCart();
        $data = $observer->getEvent()->getInfo()->toArray();

        foreach ($data as $itemId => $itemInfo) {
            if ($item = $cart->getQuote()->getItemById($itemId)) {
                $giftWrap = array_key_exists('gift-wrap', $itemInfo)? 1 : 0;
                $item->setGiftWrap($giftWrap);
            }
        }

        return $this;
    }
}
