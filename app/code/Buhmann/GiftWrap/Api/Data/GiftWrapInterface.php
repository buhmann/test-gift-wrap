<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Buhmann\GiftWrap\Api\Data;

interface GiftWrapInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    const KEY_GIFT_WRAP = 'gift_wrap';

    /**
     * Return the Gift wrap.
     *
     * @return boolean|null Gift wrap.
     */
    public function getGiftWrap();

    /**
     * Sets the product quantity.
     *
     * @param bool $id
     * @return $this
     */
    public function setGiftWrap($id);
}
