<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Buhmann\GiftWrap\Model;

/**
 * Gift Wrap model
 *
 * @api
 * @since 100.0.2
 */
class GiftWrap extends \Magento\Framework\Model\AbstractExtensibleModel implements \Buhmann\GiftWrap\Api\Data\GiftWrapInterface
{
    /**
     * {@inheritdoc}
     */
    public function getGiftWrap()
    {
        return $this->getData(self::KEY_GIFT_WRAP);
    }

    /**
     * {@inheritdoc}
     */
    public function setGiftWrap($id)
    {
        return $this->setData(self::KEY_GIFT_WRAP, $id);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return \Buhmann\GiftWrap\Api\Data\GiftWrapExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     *
     * @param \Buhmann\GiftWrap\Api\Data\GiftWrapExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(\Buhmann\GiftWrap\Api\Data\GiftWrapExtensionInterface $extensionAttributes)
    {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
